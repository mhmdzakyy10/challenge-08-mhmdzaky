"use strict";
const { Model } = require("sequelize");

// import Bcrypt
const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasOne(models.UserGameBiodata, { foreignKey: "id", as: "userGameBiodata" });
      this.hasMany(models.UserGameHistory, { foreignKey: "id", as: "userGameHistory" });
    }

    // encrypt
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    // methode register
    static register = ({ username, password }) => {
      const encryptedPassword = UserGame.#encrypt(password);

      return this.create({ username, password: encryptedPassword });
    };

    // methode authentication
    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username } });
        if (!user) return Promise.reject("User not found");

        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Password is invalid");

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    };
  }
  UserGame.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      token: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: "user_games",
      modelName: "UserGame",
      underscored: true,
    }
  );
  return UserGame;
};
